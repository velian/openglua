#pragma once

#include <vector>
#include "SFML\Graphics.hpp"

struct SpriteList
{
public:

	SpriteList()
	{
		m_names = std::vector<std::string>();
		m_sprites = std::vector<sf::Sprite*>();
	}

	~SpriteList(){}

	std::vector<std::string> m_names;
	std::vector<sf::Sprite*> m_sprites;

};