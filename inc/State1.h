#pragma once
#include "GameState.h"

class GameStateManager;
class Camera;
class LuaScript;
class State1 : public GameState
{
public:

	//Constructor, called when added to the gamestatemanager
	State1(unsigned int _id, GameStateManager* _gameStateManager);

	//Initializer, called when switching this to the active state
	virtual void Initialize();

	virtual void Update(double _deltaTime);
	virtual void Draw();

protected:

	virtual void UpdateConsole();
	virtual void DrawGUI(char* _stateName);

private:

	//State variables
	bool m_showConsole;

	//State objects go in here
	LuaScript* m_luaMain;
	Camera* m_camera;
};