#pragma once

#include <string>
#include <map>

#include "Sprite.h"

#include "SFML\OpenGL.hpp"
#include "SFML\System.hpp"
#include "SFML\Graphics.hpp"

class GameStateManager;
class Application
{
public:

	void Initialize(unsigned int _width, unsigned int _height, char* _windowTitle = nullptr);

	void Begin();
	void End();

	void Shutdown();

	void PushSprite(std::string _name, sf::Sprite*);
	void DrawSprite(std::string _name, sf::Vector2f _position, sf::Vector2f _scale);

	sf::RenderWindow* GetWindow(){return m_window;}

	static Application* Instance();

	bool m_shouldClose;

private:

	Application();
	~Application();
	Application(Application const&) = delete;

	static Application* m_instance;

	sf::RenderWindow* m_window;
	SpriteList* m_spriteList;
	GameStateManager* m_gameStateManager;
	std::string m_windowTitle;
	unsigned int m_width, m_height;
	int m_windowFlags;
	
};