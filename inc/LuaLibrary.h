#include <GLFW/glfw3.h>
#include <IMGUI/ImGui.h>
#include <vector>

#include "Application.h"

//static int LuaDrawSquareI(lua_State* _luaState)
//{
//	float positionX = (float)lua_tonumber(_luaState, 1); // get function argument
//	float positionY = (float)lua_tonumber(_luaState, 2);
//	float positionZ = (float)lua_tonumber(_luaState, 3);
//	//float extents = lua_tostring(_luaState, 3); // get function argument
//	//glm::vec4 colour = lua_tostring(_luaState, 3); // get function argument
//	//Gizmos::addAABB(glm::vec3(positionX, positionY, positionZ), glm::vec3(1), glm::vec4(1)); // calling C++ function with this argument...
//	return 0;
//}
//
//static int LuaDrawSphereI(lua_State* _luaState)
//{
//	float positionX = (float)lua_tonumber(_luaState, 1); // get function argument
//	float positionY = (float)lua_tonumber(_luaState, 2);
//	float positionZ = (float)lua_tonumber(_luaState, 3);
//	Gizmos::addSphere(glm::vec3(positionX, positionY, positionZ), 1, 16, 16, glm::vec4(1)); // calling C++ function with this argument...
//	return 0;
//}

static int LuaKeyDownI(lua_State* _luaState)
{
	int key = (int)lua_tonumber(_luaState, 1);
	return sf::Keyboard::isKeyPressed((sf::Keyboard::Key)key);
}

static int LuaLogI(lua_State* _luaState)
{
	std::string str = lua_tostring(_luaState, 1);
	ImGui::LogCustomConsole((char*)str.c_str());
	return 0;	
}

static int LuaApplicationInitialize(lua_State* _luaState)
{
	unsigned int windowWidth = (unsigned int)lua_tonumber(_luaState, 1);
	unsigned int windowHeight = (unsigned int)lua_tonumber(_luaState, 2);
	std::string windowTitle = lua_tostring(_luaState, 3);
	Application::Instance()->Initialize(windowWidth, windowHeight, (char*)windowTitle.c_str());
	return 0;
}

static int LuaApplicationBegin(lua_State* _luaState)
{
	Application::Instance()->Begin();
	return 0;
}

static int LuaApplicationEnd(lua_State* _luaState)
{
	Application::Instance()->End();
	return 0;
}

static int LuaApplicationShutdown(lua_State* _luaState)
{
	Application::Instance()->Shutdown();
	return 0;
}

static int LuaImGuiText(lua_State* _luaState)
{
	std::string text = lua_tostring(_luaState, 1);
	ImGui::Text(text.c_str());
	return 0;
}

static int LuaImGuiButton(lua_State* _luaState)
{
	std::string label = lua_tostring(_luaState, 1);	
	return ImGui::Button(label.c_str());
}

static int LuaImGuiSeperator(lua_State* _luaState)
{
	ImGui::Separator();
	return 0;
}

static int LuaImGuiConsole(lua_State* _luaState)
{
	sf::Vector2u winSize;

	winSize = Application::Instance()->GetWindow()->getSize();

	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(ImVec2((float)winSize.x, (float)winSize.y));

	ImGuiWindowFlags windowFlags = 0;
	windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
	windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoMove;
	windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoResize;
	windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoTitleBar;

	ImGui::ShowCustomConsole(NULL, windowFlags);

	return 0;
}

static int LuaImGuiConsoleCommands(lua_State* _luaState)
{
	if (ImGui::GetConsoleUpdated())
	{
		const char* command_line = ImGui::GetConsoleBuffer();
		std::string strCommandLine(command_line);

		if (strCommandLine[0] != '/')
		{
			return 0;
		}

		size_t uiSpaceLoc = 0;

		//Find first space location
		if (strCommandLine.find(' ') != std::string::npos)
		{
			uiSpaceLoc = strCommandLine.find(' ');
		}

		std::string command;
		std::vector<std::string> vars;
		int totalVars = 1;

		if (uiSpaceLoc != 0)
		{
			command = strCommandLine.substr(1, uiSpaceLoc - 1);

			int nextSpace = strCommandLine.find(' ', uiSpaceLoc + 1);

			while (uiSpaceLoc < strCommandLine.length())
			{
				vars.push_back(strCommandLine.substr(uiSpaceLoc + 1, nextSpace - uiSpaceLoc - 1));
				uiSpaceLoc = nextSpace;
				nextSpace = strCommandLine.find(' ', uiSpaceLoc + 1);
				totalVars++;
			}
		}
		else
		{
			command = strCommandLine.substr(1, strCommandLine.length());
		}

		lua_getglobal(_luaState, "HandleConsoleCommands");
		lua_pushstring(_luaState, command.c_str());

		for each(std::string variable in vars)
		{
			lua_pushstring(_luaState, variable.c_str());
		}

		lua_pcall(_luaState, totalVars, 0, 0);
	}

	return 0;
}

static int LuaGetDeltaTime(lua_State* _luaState)
{
	lua_pushnumber(_luaState, ImGui::GetIO().DeltaTime);
	return 1;
}

static int LuaSFMLCreateSprite(lua_State* _luaState)
{
	std::string name = lua_tostring(_luaState, 1);
	std::string path = lua_tostring(_luaState, 2);

	float x = (float)lua_tonumber(_luaState, 3);
	float y = (float)lua_tonumber(_luaState, 4);
	float w = (float)lua_tonumber(_luaState, 5);
	float h = (float)lua_tonumber(_luaState, 6);

	sf::Texture* tex = new sf::Texture();
	tex->loadFromFile(path);

	sf::Sprite* sprite = new sf::Sprite(*tex);
	sprite->setPosition(sf::Vector2f(x, y));
	sprite->setScale(sf::Vector2f(w, h));

	Application::Instance()->PushSprite(name, sprite);
	return 0;
}

static int LuaSFMLDrawSprite(lua_State* _luaState)
{
	std::string name = lua_tostring(_luaState, 1);

	float x = (float)lua_tonumber(_luaState, 2);
	float y = (float)lua_tonumber(_luaState, 3);
	float w = (float)lua_tonumber(_luaState, 4);
	float h = (float)lua_tonumber(_luaState, 5);

	Application::Instance()->DrawSprite(name, sf::Vector2f(x, y), sf::Vector2f(w,h));
	return 0;
}

//static int LuaClientConnectI(lua_State* _luaState)
//{
//	std::string ip;
//	std::string str;
//	unsigned int port;
//
//	RakConnection* pClient = (RakConnection*)lua_touserdata(_luaState, 1);
//	
//	if (lua_tostring(_luaState, 2) != nullptr)
//	{
//		ip = lua_tostring(_luaState, 2);
//	}
//	else
//	{
//		ip = pClient->m_ipAddress;
//	}
//
//	if (lua_tonumber(_luaState, 3) != 0)
//	{
//		port = (unsigned int)lua_tonumber(_luaState, 3);
//	}
//	else
//	{
//		port = pClient->m_port;
//	}
//	
//	if (lua_tostring(_luaState, 4) != nullptr)
//	{
//		str = lua_tostring(_luaState, 4);
//	}	
//	else
//	{
//		str = pClient->m_clientName;
//	}
//
//	pClient->Connect((char*)ip.c_str(), port, (char*)str.c_str());
//
//	return 0;
//}
//
//static int LuaClientSendFileI(lua_State* _luaState)
//{
//	std::string str;
//
//	RakClient* pClient = (RakClient*)lua_touserdata(_luaState, 1);
//
//	if (!pClient->Initialized())
//	{
//		ImGui::LogCustomConsole("You must be connected to a server before using this function.");
//	}
//
//	if (lua_tostring(_luaState, 2) != nullptr)
//	{
//		str = lua_tostring(_luaState, 2);
//	}
//	else
//	{
//		str = "./scripts/main.lua";
//		// str = pClient->m_clientName;
//	}
//
//	pClient->SendFile(/*str*/);
//
//	return 0;
//}

static const struct luaL_Reg library[] =
{
	{	"KeyDown",					LuaKeyDownI					},
	{	"Log",						LuaLogI						},
	{	"ApplicationInitialize",	LuaApplicationInitialize	},
	{	"ApplicationBegin",			LuaApplicationBegin			},
	{	"ApplicationEnd",			LuaApplicationEnd			},
	{	"ApplicationShutdown",		LuaApplicationShutdown		},
	{	"ImGuiText",				LuaImGuiText				},
	{	"ImGuiButton",				LuaImGuiButton				},
	{	"ImGuiSeperator",			LuaImGuiSeperator			},
	{	"ImGuiConsole",				LuaImGuiConsole				},
	{	"ImGuiConsoleCommands",		LuaImGuiConsoleCommands		},
	{	"DeltaTime",				LuaGetDeltaTime				},
	{	"SFMLCreateSprite",			LuaSFMLCreateSprite			},
	{	"SFMLDrawSprite",			LuaSFMLDrawSprite			},
	{ NULL, NULL }  /* sentinel */
};