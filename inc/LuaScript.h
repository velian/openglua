#pragma once

#include <LUA/lua.hpp>
#include <string>
#include <functional>
#include <Windows.h>

#if !defined LUA_VERSION_NUM
/* Lua 5.0 */
#define luaL_Reg luaL_reg
#endif

class LuaScript
{
public:

	LuaScript(char* _fileName);
	~LuaScript();	

	void RunFile();
	void RunFunction(std::string _functionName, const char* _argv = nullptr...);

	void Reload();
	void RegisterLibrary(const struct luaL_Reg* _library);

	template<typename T>
	T GetGlobal(char* _variableName);

	lua_State* m_luaState;

	bool m_loaded;
	bool m_hasChanged;

private:
	char* m_fileName;
	
	int m_time;
};