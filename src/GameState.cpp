#include "GameState.h"

GameState::GameState(unsigned int _id, GameStateManager* _gameStateManager)
{
	m_id = _id;
	m_gameStateManager = _gameStateManager;
}

GameState::~GameState()
{

}

void GameState::Initialize()
{
	printf("Initialized gamestate with ID : %i\n", m_id);
}

void GameState::Update(double _deltaTime)
{
	//Overload Update
}

void GameState::Draw()
{
	//Overload Draw
}

void GameState::DrawGUI(char* _stateName)
{
	if (ImGui::CollapsingHeader("State Settings"))
	{
		ImGui::SameLine();
		ImGui::Text("CURRENT STATE : %s", _stateName);

		if (ImGui::Button("PREVIOUS STATE")){
			m_gameStateManager->PreviousState();
		}
		ImGui::SameLine();
		if (ImGui::Button("NEXT STATE")){
			m_gameStateManager->NextState();
		}
	}
	ImGui::Separator();
}