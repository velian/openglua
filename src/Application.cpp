#include "Application.h"

#include <GLFW\glfw3.h>
#include <GL\gl_core_4_4.h>

#include "LUA\lua.hpp"

#include "IMGUI\imgui.h"
#include "IMGUI\imgui_impl_glfw_gl3.h"

#include "GameStateManager.h"

#include <string>
#include <stdio.h>
#include <Windows.h>
#include <functional>

Application* Application::m_instance = nullptr;

Application* Application::Instance()
{
	if (m_instance == nullptr)
	{
		m_instance = new Application();
	}
	return m_instance;
}

Application::Application()
{
	m_window = nullptr;
	m_gameStateManager = nullptr;
	m_shouldClose = false;
	
	m_spriteList = new SpriteList();
}

void Application::Initialize(unsigned int _width, unsigned int _height, char* _windowTitle)
{
	m_width = _width; //(_width == 0) ? m_configLua->GetGlobal<unsigned int>("screenWidth") : _width;
	m_height = _height; //(_height == 0) ? m_configLua->GetGlobal<unsigned int>("screenHeight") : _height;
	m_windowTitle = _windowTitle; //(_windowTitle == nullptr) ? m_configLua->GetGlobal<char*>("screenTitle") : _windowTitle;

	glfwWindowHint(GLFW_RESIZABLE, false);

	m_window = new sf::RenderWindow(sf::VideoMode(m_width, m_height), m_windowTitle);

	//if (glfwInit() == false)
	//{
	//	printf("Failed to initialize GLFW\n");
	//	return;
	//}

	//if (window == nullptr)
	//{
	//	printf("Window failed to initialize\n");
	//	glfwTerminate();
	//	return;
	//}

	glfwMakeContextCurrent((GLFWwindow*)m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		printf("OGL failed to initialize\n");
		glfwTerminate();
		return;
	}

	//m_gameStateManager = new GameStateManager(0);

	ImGui_ImplGlfwGL3_Init(m_window, true);

	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_BLEND);
	//glCullFace(GL_BACK);

	m_windowFlags = 0;
	m_windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
	m_windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoMove;

	ImGuiStyle& style = ImGui::GetStyle();
	style.WindowRounding = 0;
}

void Application::Begin()
{
	m_window->clear(sf::Color(50,50,50,255));
	ImGui_ImplGlfwGL3_NewFrame();
	ImGui::Begin("Debug Window", NULL, ImVec2(0, 0), -1.0f, m_windowFlags);
	ImGui::SetWindowPos(ImVec2(0, 0));
	ImGui::Text("%.3f ms/frame : (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Separator();

	sf::Event event;	

	while (m_window->pollEvent(event))
	{
		ImGui_ImplGlfwGL3_ProcessEvent(event);
		if (event.type == sf::Event::Closed)
		{
			m_window->close();
			Application::Instance()->m_shouldClose = true;
		}
	}
}

void Application::End()
{
	ImGui::End();
	ImGui::Render();

	m_window->display();

	//glfwSwapBuffers(glfwGetCurrentContext());

	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClearColor(0.5f, 0.5f, 0.5f, 1.f);
	//glfwPollEvents();

	//if (ImGui::IsKeyPressed(GLFW_KEY_ESCAPE))
	//{
	//	if (MessageBox(nullptr, TEXT("Are you sure you want to quit?"), TEXT(""), MB_YESNO) == IDYES)
	//	{
	//		glfwSetWindowShouldClose(glfwGetCurrentContext(), true);
	//	}
	//}
}

void Application::Shutdown()
{
	m_shouldClose = true;
}

void Application::PushSprite(std::string _name, sf::Sprite* _sprite)
{
	m_spriteList->m_names.push_back(_name);
	m_spriteList->m_sprites.push_back(_sprite);
}

void Application::DrawSprite(std::string _name, sf::Vector2f _position, sf::Vector2f _scale)
{
	int iterator = 0;
	for (std::string name : m_spriteList->m_names)
	{
		if (name == _name)
		{
			m_spriteList->m_sprites[iterator]->setPosition(_position);
			m_spriteList->m_sprites[iterator]->setScale(_scale);

			m_window->draw(*m_spriteList->m_sprites[iterator]);
			break;
		}

		iterator++;
	}
}

Application::~Application()
{
	ImGui_ImplGlfwGL3_Shutdown();
	glfwDestroyWindow(glfwGetCurrentContext());
	glfwTerminate();
}