#include <GLFW\glfw3.h>
#include <GL\gl_core_4_4.h>

#include "State1.h"

#include "LuaScript.h"

State1::State1(unsigned int _id, GameStateManager* _gameStateManager) : GameState(_id, _gameStateManager)
{
	m_camera = nullptr;
	m_showConsole = false;
}

void State1::Initialize()
{
	//m_camera = new Camera(3.14159f * 0.25f, 4.0f / 3.0f, 0.01f, 100.f);
	//m_camera->setSpeed(0);
	//m_camera->setLookAtFrom(glm::vec3(0, 5, -20), glm::vec3(0, 0, 0));

	//m_luaMain = new LuaScript("./scripts/main.lua");

	//lua_pushlightuserdata(m_luaMain->m_luaState, m_client);
	//lua_setglobal(m_luaMain->m_luaState, "connection");
}

void State1::Update(double _deltaTime)
{
	//m_camera->update((float)_deltaTime);

	m_luaMain->RunFile();
	m_luaMain->RunFunction("main", "");

	UpdateConsole();

	if (ImGui::IsKeyPressed('`', false))
	{
		m_showConsole = !m_showConsole;
	}
}

void State1::UpdateConsole()
{
	if (ImGui::GetConsoleUpdated())
	{
		const char* command_line = ImGui::GetConsoleBuffer();
		std::string strCommandLine(command_line);

		if (strCommandLine[0] != '/')
		{
			return;
		}

		size_t uiSpaceLoc = 0;

		//Find first space location
		if (strCommandLine.find(' ') != std::string::npos)
		{
			uiSpaceLoc = strCommandLine.find(' ');
		}

		std::string command;
		std::vector<std::string> vars;
		int totalVars = 1;

		if (uiSpaceLoc != 0)
		{
			command = strCommandLine.substr(1, uiSpaceLoc - 1);

			int nextSpace = strCommandLine.find(' ', uiSpaceLoc + 1);

			while (uiSpaceLoc < strCommandLine.length())
			{
				vars.push_back(strCommandLine.substr(uiSpaceLoc + 1, nextSpace - uiSpaceLoc - 1));
				uiSpaceLoc = nextSpace;
				nextSpace = strCommandLine.find(' ', uiSpaceLoc + 1);
				totalVars++;
			}
		}
		else
		{
			command = strCommandLine.substr(1, strCommandLine.length());
		}

		lua_getglobal(m_luaMain->m_luaState, "HandleConsoleCommands");
		lua_pushstring(m_luaMain->m_luaState, command.c_str());

		for each(std::string variable in vars)
		{
			lua_pushstring(m_luaMain->m_luaState, variable.c_str());
		}

		lua_pcall(m_luaMain->m_luaState, totalVars, 0, 0);
	}
}

void State1::Draw()
{
	DrawGUI("State 1");
}

void State1::DrawGUI(char* _stateName)
{
	GameState::DrawGUI(_stateName);

	if (m_showConsole)
	{
		int width, height;
		glfwGetWindowSize(glfwGetCurrentContext(), &width, &height);

		ImGui::SetNextWindowPos(ImVec2(0, 0));
		ImGui::SetNextWindowSize(ImVec2((float)width, (float)height));

		ImGuiWindowFlags windowFlags = 0;
		windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse;
		windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoMove;
		windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoResize;
		windowFlags |= ImGuiWindowFlags_::ImGuiWindowFlags_NoTitleBar;

		ImGui::ShowCustomConsole(NULL, windowFlags);
	}
}