#include "LuaScript.h"
#include "Application.h"

int main(int a_argc, char** a_argv)
{
	LuaScript* applicationScript = new LuaScript("./scripts/application.lua");

	std::vector<LuaScript*> hotloadFiles = std::vector<LuaScript*>();
	hotloadFiles.push_back(new LuaScript("./scripts/keybindings.lua"));
	hotloadFiles.push_back(new LuaScript("./scripts/sprite.lua"));
	hotloadFiles.push_back(new LuaScript("./scripts/console.lua"));
	hotloadFiles.push_back(new LuaScript("./scripts/playState.lua"));

	if (!applicationScript->m_loaded) return -1;

	while (!Application::Instance()->m_shouldClose)
	{
		applicationScript->RunFile();

		for (LuaScript* script : hotloadFiles)
		{
			script->RunFile();

			if (script->m_hasChanged)
			{
				applicationScript->RunFunction("reloadScripts");
			}
		}
		
		applicationScript->RunFunction("run");		
	}
}