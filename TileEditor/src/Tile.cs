﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TileEditor
{
    class Tile
    {
        public Image m_image;

        public int x, y;
        public int w, h;

        public Tile(int _x, int _y, int _width, int _height, string _uriSource, Grid _renderGrid)
        {
            EventManager.RegisterClassHandler(typeof(Window), Window.MouseLeftButtonDownEvent, new MouseButtonEventHandler(HandleClick));

            m_image = new Image();

            x = _x;
            y = _y;

            w = 50;
            h = 50;

            m_image.Width = w;
            m_image.Height = h;

            BitmapImage bitmapImg = new BitmapImage();
            bitmapImg.BeginInit();
            bitmapImg.UriSource = new Uri(_uriSource, UriKind.Relative);
            bitmapImg.DecodePixelWidth = 50;
            bitmapImg.DecodePixelHeight = 50;
            bitmapImg.EndInit();

            m_image.Source = bitmapImg;

            //_renderGrid.Margin.Left = x;
            //_renderGrid.Margin.Top = y;

            _renderGrid.Children.Add(m_image);
        }

        public void HandleClick(object sender, MouseButtonEventArgs e)
        {
            //Handle click on image
            if (e.GetPosition(m_image).X < x + w && e.GetPosition(m_image).X > x &&
                e.GetPosition(m_image).Y < y + h && e.GetPosition(m_image).Y > y)
            {
                //Change state in here to something else if we want to.
            }
        }
    }
}
