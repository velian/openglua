﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TileEditor
{
    public partial class MainWindow : Window
    {
        private void MenuNewClick(object _sender, RoutedEventArgs _eventArgs)
        {
            // Handle the creation of a new Lua file with default tile values.
            NewSettings popup = new NewSettings(this);
            popup.Show();
        }

        private void MenuOpenClick(object _sender, RoutedEventArgs _eventArgs)
        {
            //Image img = new Image();
            //
            //img.Width = 50;
            //img.Height = 50;
            //
            //BitmapImage bitmapImg = new BitmapImage();
            //bitmapImg.BeginInit();
            //bitmapImg.UriSource = new Uri("res/TileImage.png", UriKind.Relative);
            //bitmapImg.DecodePixelWidth = 50;
            //bitmapImg.DecodePixelHeight = 50;
            //bitmapImg.EndInit();
            //img.Source = bitmapImg;
            //
            //stackPanel.Children.Add(img);
        }
    }
}
