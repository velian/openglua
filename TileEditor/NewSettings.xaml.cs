﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TileEditor
{
    /// <summary>
    /// Interaction logic for NewSettings.xaml
    /// </summary>
    public partial class NewSettings : Window
    {
        private MainWindow m_mainWindow;

        public NewSettings(MainWindow _mainWindow)
        {
            m_mainWindow = _mainWindow;
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            rowsNumber.Content = e.NewValue.ToString();
        }

        private void Slider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            colsNumber.Content = e.NewValue.ToString();
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            m_mainWindow.CreateGrid(rowsSlider.Value, colsSlider.Value);
            Close();
        }
    }
}
