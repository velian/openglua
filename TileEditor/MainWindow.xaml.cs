﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TileEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LuaScript luaScript = new LuaScript();
        }

        public void CreateGrid(double _rows, double _cols)
        {
            renderGrid.Children.Clear();

            for(int i = 0; i < _rows; i++)
            {
                for(int j = 0; j < _cols; j++)
                {
                    Tile tile = new Tile(i * 50, j * 50, 50, 50, "res/TileImage.png", renderGrid);
                }
            }

            renderGrid.UpdateLayout();
        }
    }
}
