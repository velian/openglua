--app.Log("Loaded Console Commands");

local ConsoleCommands =
{
	--["clientSendFile"] = ClientSendFile,
	--["Main"] = main,
	--["AddRigidbody"] = AddRigidbody,
	--["SetTimestep"] = SetTimestep,
	--Other functions here
}

function HandleConsoleCommands(command, ...)
	if ConsoleCommands[command] == nil then
		app.Log("Invalid console command - '" .. command .. "'");
		return;
	end
	ConsoleCommands[command](...);
end