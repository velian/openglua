--app.Log("Loaded Sprite");

function CreateSprite(name, path, x, y, w, h)

	local sprite = {};
	
	sprite.name = name;
	sprite.path = path;

	sprite.x = x;
	sprite.y = y;
	sprite.w = w;
	sprite.h = h;

	function DrawSprite()	
		app.SFMLDrawSprite(sprite.name, sprite.x, sprite.y, sprite.w, sprite.h);
	end

	sprite.DrawSprite = DrawSprite;

	app.SFMLCreateSprite(sprite.name, sprite.path, sprite.x, sprite.y, sprite.w, sprite.h);

	return sprite;

end

