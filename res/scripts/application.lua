require "scripts.config"
require "scripts.console"
require "scripts.keybindings"

local function init()
	app.ApplicationInitialize(800, 600, "openglua");
	APPLICATION_INITIALIZED = true;

	--testState = require "scripts.test_state"
	playState = require "scripts.playState"
end

local function update()

	--Handle console trigger
	if app.KeyDown(Key.Tilde) and not APPLICATION_CONSOLE_PRESSED then
		APPLICATION_SHOW_CONSOLE = not APPLICATION_SHOW_CONSOLE;
		APPLICATION_CONSOLE_PRESSED = true;
	elseif not app.KeyDown(Key.Tilde) and APPLICATION_CONSOLE_PRESSED then
		APPLICATION_CONSOLE_PRESSED = false;
	end

	--Handle reloading from application
	if app.KeyDown(Key.F5) then
		reloadScripts();
	end

	--Handle close applicaiton
	if app.KeyDown(Key.Escape) then
		app.ApplicationShutdown();
	end

	--testState:update();
	playState:update();

end

local function draw()
	app.ApplicationBegin();

	playState:draw();

	if APPLICATION_SHOW_CONSOLE then
		app.ImGuiConsole();
		app.ImGuiConsoleCommands();
	end

	app.ApplicationEnd();
end

function run()

	if APPLICATION_INITIALIZED == false then
		print("First init...");
		init();
		print("Succeeded first init.");
	end

	update();
	draw();
end

function reloadScripts()
	print("Reloading scripts... ");
	--testState = dofile("scripts/test_state.lua");
	--testState:init();

	playState = dofile("scripts/playState.lua");
	playState:init();

end