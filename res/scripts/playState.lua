require "scripts.keybindings"
require "scripts.sprite"

local playState = {};

background = CreateSprite("Background", "./images/background.jpg", 0, 0, 1.34, 1.5);

function playState.init(self)
	app.Log("Initialized the Main State");
end

function playState.update(self)

end

function playState.draw(self)
	background:DrawSprite();
end

return playState;