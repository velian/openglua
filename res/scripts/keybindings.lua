app.Log("Loaded Keybindings");

--SFML KEYBINDINGS--
Key = {};

Key.Unknown 	= -1;
Key.A			= 0;        
Key.B			= 1;            
Key.C			= 2;            
Key.D			= 3;            
Key.E			= 4;            
Key.F			= 5;            
Key.G			= 6;            
Key.H			= 7;            
Key.I			= 8;            
Key.J			= 9;            
Key.K 			= 10;            
Key.L 			= 11;            
Key.M 			= 12;            
Key.N 			= 13;            
Key.O 			= 14;            
Key.P 			= 15;            
Key.Q 			= 16;            
Key.R 			= 17;            
Key.S 			= 18;            
Key.T 			= 19;            
Key.U 			= 20;            
Key.V 			= 21;            
Key.W 			= 22;            
Key.X 			= 23;            
Key.Y 			= 24;            
Key.Z 			= 25;            
Key.Num0 		= 26;         
Key.Num1 		= 27;         
Key.Num2 		= 28;         
Key.Num3 		= 29;         
Key.Num4 		= 30;         
Key.Num5 		= 31;         
Key.Num6 		= 32;         
Key.Num7 		= 33;         
Key.Num8 		= 34;         
Key.Num9 		= 35;         
Key.Escape 		= 36;       
Key.LControl 	= 37;     
Key.LShift 		= 38;       
Key.LAlt 		= 39;         
Key.LSystem 	= 40;      
Key.RControl 	= 41;     
Key.RShift 		= 42;       
Key.RAlt 		= 43;         
Key.RSystem 	= 44;      
Key.Menu 		= 45;         
Key.LBracket 	= 46;     
Key.RBracket 	= 47;     
Key.SemiColon 	= 48;    
Key.Comma 		= 49;        
Key.Period 		= 50;       
Key.Quote 		= 51;        
Key.Slash 		= 52;        
Key.BackSlash 	= 53;    
Key.Tilde 		= 54;        
Key.Equal 		= 55;        
Key.Dash 		= 56;         
Key.Space 		= 57;        
Key.Return 		= 58;       
Key.BackSpace 	= 59;    
Key.Tab 		= 60;          
Key.PageUp 		= 61;       
Key.PageDown 	= 62;     
Key.End 		= 63;          
Key.Home 		= 64;         
Key.Insert 		= 65;       
Key.Delete 		= 66;       
Key.Add 		= 67;          
Key.Subtract 	= 68;     
Key.Multiply 	= 69;     
Key.Divide 		= 70;       
Key.Left 		= 71;         
Key.Right 		= 72;        
Key.Up 			= 73;           
Key.Down 		= 74;         
Key.Numpad0 	= 75;      
Key.Numpad1 	= 76;      
Key.Numpad2 	= 77;      
Key.Numpad3 	= 78;      
Key.Numpad4 	= 79;      
Key.Numpad5 	= 80;      
Key.Numpad6 	= nil;      
Key.Numpad7 	= nil;      
Key.Numpad8 	= nil;      
Key.Numpad9 	= nil;      
Key.F1 			= nil;           
Key.F2 			= nil;           
Key.F3 			= nil;           
Key.F4 			= nil;           
Key.F5 			= 89;           
Key.F6 			= nil;           
Key.F7 			= nil;           
Key.F8 			= nil;           
Key.F9 			= nil;           
Key.F10 		= nil;          
Key.F11 		= nil;          
Key.F12 		= nil;          
Key.F13 		= nil;          
Key.F14 		= nil;          
Key.F15 		= nil;          
Key.Pause 		= nil;
