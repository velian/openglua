require "scripts.keybindings"
require "scripts.sprite"

local test_state = {};

testSprite = CreateSprite("Test", "./images/test.png", 50, 50, 0.5, 0.5);
testSprite2 = CreateSprite("Test2", "./images/test.png", 0, 0, 0.5, 0.5);

function test_state.init(self)
	app.Log("Initialized the Test State");
end

function test_state.update(self)

	if app.KeyDown(Key.W) then
		testSprite.y = testSprite.y - 2 * app.DeltaTime();
	end

	if app.KeyDown(Key.A) then
		testSprite.x = testSprite.x - 2 * app.DeltaTime();
	end

	if app.KeyDown(Key.S) then
		testSprite.y = testSprite.y + 2 * app.DeltaTime();
	end

	if app.KeyDown(Key.D) then
		testSprite.x = testSprite.x + 2 * app.DeltaTime();
	end
end

function test_state.draw(self)
	testSprite:DrawSprite();
	testSprite2:DrawSprite();
end

return test_state;